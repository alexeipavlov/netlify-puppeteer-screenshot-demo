const chromium = require("chrome-aws-lambda");
const admin = require("firebase-admin");
const serviceAccount = require("./alert-way-firebase-adminsdk-bmfl0-03dfbf5eee.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://alert-way.firebaseio.com",
});
const CRED = require("./creds");
const sleep = async (ms) => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res();
    }, ms);
  });
};

const ID = {
  login: "#email",
  pass: "#pass",
};

const extractItems = () => {
  let data = [];
  let elements = document.getElementsByClassName(
    "_4-u2 mbm _4mrt _5jmm _5pat _5v3q _7cqq _4-u8"
  );
  for (var element of elements) {
    data.push({
      image: element.getElementsByClassName(
        "_s0 _4ooo _6y97 _6_ut _5xib _5sq7 _44ma _rw"
      )[0]
        ? element.getElementsByClassName(
            "_s0 _4ooo _6y97 _6_ut _5xib _5sq7 _44ma _rw"
          )[0].src
        : null,
      time: element.querySelector("abbr").title,
      content: element.innerText.split("\n\n")[1],
      author: element.innerText.split("\n")[0],
      id: element.getElementsByClassName("_5pcq")[0].href,
    });
  }
  return data;
};

const getItems = async (page, extractItems) => {
  const items = await page.evaluate(extractItems);
  return items;
};

const retrievePosts = async (group) => {
  const browser = await chromium.puppeteer.launch({
    args: chromium.args,
    defaultViewport: chromium.defaultViewport,
    executablePath: await chromium.executablePath,
    headless: chromium.headless,
  });
  const page = await browser.newPage();
  let login = async () => {
    await page.goto("https://facebook.com", {
      waitUntil: "networkidle2",
    });
    await page.waitForSelector(ID.login);
    await page.type(ID.login, CRED.user);
    await page.type(ID.pass, CRED.pass);
    await sleep(1000);
    await page.click("#loginbutton");
    console.log("login done");
    await page.waitForNavigation();
  };
  await login();

  await page.goto(`https://www.facebook.com/groups/${group}`, {
    waitUntil: "networkidle2",
  });

  const items = await getItems(page, extractItems);
  await browser.close();
  return items;
};

exports.handler = async (event, context) => {
  const items = await retrievePosts("292458127505205");
  console.log(items);
  /* const db = admin.firestore();
  const batch = db.batch();
  items.forEach((doc) => {
    batch.set(
      db
        .collection("alerts")
        .doc(
          "292458127505205_" + doc.id.split("/")[doc.id.split("/").length - 2]
        ),
      doc
    );
  });
  await batch.commit();*/

  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Complete screenshot of ${pageToScreenshot}`,
      buffer: screenshot,
    }),
  };
};
